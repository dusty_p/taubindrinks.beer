---
title: Hydrometer reading
author: Dustin Plunkett
type: post
date: 2013-04-22T02:20:28+00:00
categories:
  - Beer
  - Blog
  - Homebrewing
tags:
  - brewing
  - Homebrew
---

It's been 9 days, and I figured it was time to take my first hydrometer reading. After a quick run to the local homebrew shop to pick up some supplies, I rushed home to take the reading. We'll get to that in a moment, but, first, let me tell you about the most amazing piece of equipment I have picked up for this endeavour yet: the <a title="Auto-Easy Siphon " href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/765-auto.html" target="_blank">Auto-Siphon</a>. This little tube with some plastic bits, is amazing.

<!--more-->

Using this little guy, I was able to siphon off enough beer to fill the hydrometer testing tube (after sterilising the siphon of course). It took all of 10 seconds to start it up, and siphon it out. There was a tiny mess (don't tell my wife) but it was easy to clean up. This will also make things a lot easier when moving the beer from the fermentor into the bottling barrel. Once there, it will be easy to fill using a bottle filler that I also picked up. Anyways, that's all in the future, and there will be posts on bottling day of course.

For now, it's back to the hydrometer reading. What this does, is test the gravity (or density) of the beer. This allows you to test how much alcohol is present in the beer as well. Different beers end at different gravities. So, what makes this reading so important? For one, it's one of the best ways to tell when the beer is ready for bottling. You take a reading, write it down, wait two days and take another one. If they are the same, it's bottling time! If not, wait a few more days and try again. Rinse and repeat as needed.

<a title="Hydrometer reading by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8670718166_aaa20799cf.jpg"><img title="Hydrometer reading" alt="Hydrometer reading" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8670718166_aaa20799cf.jpg" width="500" height="333" /></a> Hydrometer reading: 1.011
<p style="text-align: left;">This all brings me to the first gravity reading using the hydrometer. It came out to 1.011 or so. This puts it pretty close to being where the final gravity should be for this beer. It should be just about finished fermenting! Exciting times ahead! Keep tuned for further updates on my first homebrew.</p>


