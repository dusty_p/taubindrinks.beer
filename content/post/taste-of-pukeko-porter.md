---
title: Taste of Pukeko Porter
author: Dustin Plunkett
type: post
date: 2013-08-26T20:59:58+00:00
categories:
  - Beer
  - Homebrewing
---

Today I decided to take my first taste of Pukeko Porter. My second homebrew. This one was made with the <a title="Mangrove Jacks Craft Series Box" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/night-watchman-brewery-box1" target="_blank">Mangrove Jack's craft series box</a>. How did it turn out? Good? Bad? Undrinkable? Best beer ever? See for yourself:

<!--more-->

[caption id="attachment_457"width="266"]<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/ppclipped.png"><img class="size-medium wp-image-457" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/ppclipped.png" alt="Pukeko Porter label" width="266" height="300" /></a> Pukeko Porter label

{{< youtube rvREiePf9sQ >}}

I would also like to take a moment to send my condolences to the team at Stone Brewing. One of their coworkers was killed when a fork truck he was operating rolled over on top of him. My condolences to his family and friends. The family has requested his name not be released and I will respect that. Rest in Peace sir.


