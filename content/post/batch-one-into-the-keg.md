---
title: Batch one – Into the keg
author: Dustin Plunkett
type: post
date: 2014-10-03T19:59:50+00:00
categories:
  - Beer
  - Blog
  - Homebrewing
  - Kegging
tags:
  - Beer
  - Blog
  - brewing
  - Craft beer
  - Homebrew
  - Keg
---
That's right, it's been kegged. My amazing wife allowed me to pick up a kegerator and keg. I couldn't be more excited. I've not kegged beer before, but it turned out to be amazing. Instead of spending all day bottling beer, I spent all day putting together the kegerator with the help of my brother in law. It was a pretty easy build, as it's a prebuilt kegerator with a dual tower, but it was still a little time consuming due to grabbing the wrong tools.

Once the correct tools were grabbed, everything went really smoothly. The entire build process involved basically pulling everything out of the box, putting the casters on, running some lines, and attaching the tap tower. Then just crimping everything and bam, it was ready to have C02 hooked up and beer added.

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_170529.jpg"><img class="wp-image-851 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_170529.jpg" alt="Kegeratorator being built" width="474" height="355" /></a> Putting together the kegerator

The unit I picked up is quite nice and came with everything needed to keg, except for the C02 bottle, which I had to pick up separately. It has a nice holder on the back of the fridge, to hold the C02 bottle, and run the lines nicely into the fridge.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_144734.jpg"><img class="size-large wp-image-850" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_144734.jpg" alt="Bottle Holder" width="474" height="355" /></a> The holder on the back of the fridge is very nice

The only tools we had forgotten completely, were crimping tools for the clasps that are included. Eventually these will be replaced with standard clamps, which will make disassembly easier for cleaning.

It didn't take too long to get everything put together, and really the hardest part was putting the lines over the connectors and clamping them in.  A bit of hot water took care of that. I then cleaned the reconditioned keg I purchased for use with the system. It was very clean but I made sure to dismantle everything and give it a good scrubbing.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_144810.jpg"><img class="size-large wp-image-853" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_144810.jpg" alt="Keg parts getting a soak" width="474" height="355" /></a> Keg parts being soaked

Once everything was put together, I put some cleaner into the keg and gave it a good shake and roll around. Then I hooked it up to the lines, and ran it through the taps to clean out any extra gunk under pressure. It worked quite well cranking the pressure up to 10 PSI and running it through.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_170536.jpg"><img class="size-large wp-image-859" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_170536.jpg" alt="Running cleaner" width="474" height="355" /></a> Running cleaner through the system under pressure

I then performed the same with sanitizer. While doing this, we had a little visitor come up to the garage where we were working to check things out. He seemed to approve of what was happening.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_173410.jpg"><img class="size-large wp-image-860" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_173410.jpg" alt="Pukeko" width="474" height="355" /></a> Our little pukeko visitor

He was quite friendly and came within 5 feet of me. We think the neighbor has been hand feeding him, which I don't mind at all.

Now that everything was cleaned and sanitised, I brought the keg upstairs to where my fermentation bucket was, sprayed everything down, took my final gravity reading to be certain the beer was finished fermenting, and transferred the beer into the keg. Unfortunately, I didn't get photos of the beer going into the keg, only because it was so quick! It took less time to transfer the beer into the keg, than it did putting everything together.

The keg was then hauled back downstairs, into the garage and hooked up to lines. I purged all of the oxygen out of the keg using C02, and have cranked the psi up to 30 for the next 24 hours. That will force carb the beer, and it should be ready to drink soon after.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_180733.jpg"><img class="size-large wp-image-862" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/10/IMG_20141003_180733.jpg" alt="Batch One in the keg" width="474" height="355" /></a> Batch One in the keg cooling

Overall, this process was a lot easier than doing 50 bottles of beer. I look forward to brewing my next batch, and putting it into a keg alongside this one, and having two tasty beers on tap, ready to be drank by anyone that happens to come by.

My next update will be pouring and tasting the beer, I can't wait! Until then, drink good beer.

