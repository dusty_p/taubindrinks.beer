---
title: Taubin…. Brews?
author: Dustin Plunkett
type: post
date: 2013-04-13T08:35:53+00:00
categories:
  - Homebrewing
tags:
---

That's right, Taubin doesn't just drink, Taubin is now brewing. Granted, I am just using a basic brew kit, but, it's a start. Everyone has to start someplace right? Hopefully things will go well. I mean, now all I have to do is wait, and stare at the carboy while I wait... Using the kit is incredibly easy, it's pretty much like this:

<!--more-->

1. Clean all the things

2.

<img class="alignnone size-full wp-image-191" alt="sanitize" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/sanitize2.jpg" width="320" height="240" />

3. Put kit mix into the carboy with boiling water

4. Stir the crap out of it

5. Add cold water, brewing sugar, and yeast

6. Cap everything

7.

<img class="alignnone size-full wp-image-191" alt="sanitize" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/sanitize2.jpg" width="320" height="240" />

8. Wait... And wait... And wait...

&nbsp;

I'm currently on the waiting phase of things. Hopefully in a few weeks, I'll be able to post about my new beer!

I'll be keeping up here with the progress, and posting photos as I go along. With any luck, I'l be doing this as a hobby for a while to come.

&nbsp;

Obligatory

photos of what I've done so far:

The kit:

<img alt="Brewkit" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/kit1.jpg" width="800" height="534" /> Brewkit

From the back to the front:

Brewing spoon
Carboy
Brewing Kit (English bitter)
Cleaner (Pink)
Sanitizer (White)
Brewing sugar
Hydrometer
Thermometer
Books (Homebrewers Companion and Joy of Homebrewing)

&nbsp;

And, after all the cooking sanitizing, adding and everything else, the waiting:

<img alt="The waiting begins!" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/wait.jpg" width="361" height="800" /> Waiting

&nbsp;

So, we'll see how it goes! Hopefully it's great, but, even if it's just 'meh' it will be great because I created it!

