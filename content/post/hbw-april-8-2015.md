---
title: HBW April 8 2015 – Pumpkin Spice Milk Stout
author: Dustin Plunkett
type: post
date: 2015-04-09T06:52:00+00:00
categories:
  - Beer
  - Beer Porn
  - HBW
tags:
  - Beer
  - brewing
  - Craft beer
  - HBW
  - Homebrew
  - Homebrew Wednesday
  - Taubin Drinks
---
In this episode of HBW, I talk about my latest beer, and try out a Pumpkin Spice Milk Stout from Belching Beaver Brewery. My current homebrew seems to be stuck at 1.022, which seems pretty high. It started out at 1.069 which would make it 6.17% if my calculations were stop on. I&#8217;ve just never had a beer finish that high. I may just keg it and see how it goes, it doesn&#8217;t taste overly sweet or anything. The cold brew coffee has been put in the fridge, and will be added about 24 hours prior to kegging. I&#8217;ll update on the next HBW video as to how it all goes.

<!--more-->

{{< youtube olSNNiO8fzM >}}

&nbsp;

The Pumpkin Spice Stout was very good. I&#8217;ve tried a few pumpkin beers in the past, and haven&#8217;t always been a fan, but this one is pretty darned amazing. The beer didn&#8217;t have that way overdone spice that a lot of pumpkin ales have. Overall, I would recommend it to anyone that is able to find it. Until next time, drink good beer! Cheers!

