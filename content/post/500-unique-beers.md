---
title: 3 1/2 years, 1114 beers, 500 unique beers, 1 Incredible Journey
author: Dustin Plunkett
type: post
date: 2014-04-09T03:57:00+00:00
categories:
  - Beer
  - Beer Porn
  - Blog
tags:
  - Beer
  - Blog
  - Craft beer
---


At precisely 1:15 PM on November 26th 2010, I <a title="My first beer on Untappd" href="/images/2014/04/5435" target="_blank">began a journey</a>. A journey of beer exploration. It began with a Stone Brewing Vertical Epic 10-10-10, and has continued through the years, to include many breweries, and many many beers. Some of those beers have been <a title="Not a fav" href="/images/2014/04/46947" target="_blank">pretty bad</a>, others have been <a title="So good!" href="/images/2014/04/6908">really amazing</a>, but one thing has been consistent, and that has been my beer growth.

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/lukcy.jpg"><img class="size-medium wp-image-503" alt="Lukcy Basartd" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/lukcy.jpg" width="181" height="300" /></a> Lukcy Basartd Ale

I vividly remember my first trip to a brewery. I had flown into San Diego to meet the women that would become my wife, and she had invited me to head to <a title="Stone Brewing Company" href="/images/2014/04/www.stonebrew.com" target="_blank">Stone Brewing</a> in North County. Now, I would love to tell you that I went out there, drank all the amazing beer and loved it all, however the truth is I sent my first Stone IPA back. I'll let that sink in for a moment. That's right, I went to what is now my favorite brewery on earth, and I sent one of their iconic beers back. I simply couldn't handle the hops.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/beers.jpg"><img class="size-medium wp-image-505" alt="Yay Beer" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/beers.jpg" width="300" height="200" /></a> Yay Beer

A lot has changed since then. I now love IPA's, as well as many other styles of beer. Some hoppy some not so hoppy. I have also learned a lot on my beer journey, and even create a few beers myself. I now appreciate the work and love that goes into creating the amazing beverage that is beer. You can taste the love and care that brewers put into the their beer. Whether that beer be a super hoppy IPA, a malty scotch ale, or a tangy sour, the love is there. It's one of the things that makes craft beer so great.

I've had some beers that were far from great. Some that others absolutely loved that I hated. Case in point would be Dogfish Head's Midas Touch. I personally can't stand it but a lot of people love it. But no matter what, I've never turned down the chance to try something new, especially from a local brewery.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/Midas.jpg"><img class="size-medium wp-image-504" alt="Midas Touch" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/Midas.jpg" width="225" height="300" /></a> Midas Touch

I've also been lucky enough to try many beers that <a title="California Gold XPA" href="/images/2014/04/73307" target="_blank">most people will never have tried, or even seen</a>. I've been to some of the best breweries in the west coast, as well as New Zealand, had some amazing beer and made a few friends a long the way. Where do I go from here? Well, there are tons more beers I've never tried, and new ones being made every day. There are new breweries being born out of people's passion all the time.  Not to mention my own beers, which I will continue to make.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/canonfodder.jpg"><img class="size-medium wp-image-507" alt="Canon Fodder" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/canonfodder.jpg" width="201" height="300" /></a> My very first homebrew, Canon Fodder

There are many many many more breweries to visit, and many many beers yet to try. One thing will not change though, and that is my love of craft beer, and the brewers that make them. I may not love every craft beer out there, but I'll be damned if I'll stop trying them.  And I think you, no matter who you are, should do the same. Get out, find a local brewery, and give it a shot! Even if you've never drunk anything other than Budweiser, you might just surprise yourself at what's out there.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/Hallertau.jpg"><img class="size-medium wp-image-510" alt="Hallertau Resurrection " src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/Hallertau.jpg" width="300" height="200" /></a> Hallertau Resurrection

I've always said, there is a beer for everyone's taste. Be it a dark rich chocolate stout, a sweet and sour lambic, or a very bitter IPA, I promise you, there is a beer you will love, you just have to find it. Don't be afraid to get out there and meet your local brewer. Ask him what he suggests you should try, and if you don't like it, don't be afraid to let them know. They'd much rather help you find a beer you like than to give you something you don't. Just get out there and try it!


