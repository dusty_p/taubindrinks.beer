---
title: Batch One Mistakes
author: Dustin Plunkett
type: post
date: 2014-10-05T03:25:09+00:00
categories:
  - Beer
  - Homebrewing
  - Kegging
  - Mistakes
tags:
  - Beer
  - Blog
  - brewing
  - Homebrew
  - Kegging
  - Mistakes
  - Taubin Brews Episode
---


The great thing about a new hobby, is the learning process. Part of that, is making mistakes. I mean everyone screws up, especially when trying new things. Well, I made two mistakes with my first all grain beer. Both while brewing it, as well as during kegging.

Luckily for me, and whoever gets to actually try the beer, neither are so horrible they will ruin the beer. I will learn from these mistakes, and get better next time. So here are the two mistakes (among many mistakes I'm sure) that I made while brewing and kegging Batch One:

<!--more-->
First mistake - forcing carbonation at 30 psi overnight while keeping the beer line attached

{{< youtube 84mBuxsSZmQ >}}

Luckily, it wasn't too bad, and I didn't end up with beer all over the place. In fact, nothing bad happened at all.

My second mistake was not using a hop bag during the boil, as well as not using one while dry hopping. This is a bit more of a drastic error, as it is clogging my beer line and causing me grief.

{{< youtube 0Jsebp0KhEQ >}}

I'll see how this goes, currently the beer is sitting at serving temp and pressure (10 psi). I'll let it rest a while and see how we end up. Worst case, it will be costly in terms of beer wasted (which is already too high I feel) as well as in C02 loss. Either way, it's a mistake I won't make again. I'll be building a hop spider for my boil, and getting hop bags for my dry hopping.

I'll also be looking into a keg filter to get rid of this issue when transferring into the keg in the first place. Until my next update, stay safe and drink good beer.

