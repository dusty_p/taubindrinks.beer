---
title: Moving to all grain brewing
author: Dustin Plunkett
type: post
date: 2014-09-09T05:10:01+00:00
categories:
  - Beer
  - Blog
  - Homebrewing
tags:
  - All Grain
  - Blog
  - brewing
  - Grainfather
---
I have some very exciting news: I'm moving to all grain brewing! For those of you that don't know what all grain brewing is compared to what I've been doing, it's basically going to allow me to have full control over the entire brewing process. I will be able to create my own recipes, change the types of grains that are used in the process, as well as everything else involved in the brewing process.

My long term goal here, is to move into a small shop where I can brew full time, and make a living. That's of course a very long way off, but you can't go for your dreams if you don't have any dreams to work toward.  I hold no illusions about how much hard work it will take, and how far off that is, however it is a goal and I will reach it.

<!--more-->

On to the system that I'll be using to brew my beers. Living in an apartment in New Zealand, I have a difficult time finding all of the materials needed to perform all grain brewing. The pots, tanks, cooling equipment etc.. is at best, difficult to find, and if you do find it, is often cost prohibitive.

That being said, there is light at the end of the tunnel! Early last year, I was asked to speak with a local company about my experiences brewing, and what input I might have for an upcoming product of theirs. The product is an all-in-one all grain brewing system! My feedback was pretty limited, as I've not done all grain brewing before, however, I did give them all the feedback I could.

It was quite a long time before I heard anything more about their product, but my patience was rewarded when I heard about The <a title="The Grainfather" href="http://www.grainfather.co.nz/" target="_blank">Grainfather</a>. Not only does it have an epic name, it finally brings to New Zealand an affordable, all-in-one all grain brewing system.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/the_grainfather.jpg" target="_blank"><img class="wp-image-786 aligncenter" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/_d_improd_/the_grainfather_f_improf_176x358.jpg" alt="The Grainfather" width="176" height="358" data-mce-width="176" data-mce-height="358" /></a>

The system includes everything needed to mash, sparge, boil, cool and finally transfer to a fermenter for fermentation. To say I'm excited about this system, would be a tremendous understatement. I've always been fascinated by the technique that goes into doing all grain brewing, now I get to experience that first hand. Once the system has shipped (hopefully soon!) and arrives, you can expect plenty of photos, and video while I learn the system and start my foray into all grain brewing! I get a smile on my face just thinking about it.

Until then, stay tuned, and drink good beer. Cheers!

