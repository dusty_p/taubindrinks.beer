---
title: Guy finds his house plumbed with beer… Yeah right!
author: Dustin Plunkett
type: post
date: 2013-09-17T05:39:11+00:00
categories:
  - Beer
  - Blog
tags:
  - Beer
  - Blog
  - brewing
  - Marketing
  - New Zealand Beer
  - Rant
---


So I'm a pretty avid reader of <a title="reddit: The front page of the internet" href="/images/2013/09/reddit.com" target="_blank">Reddit</a>, and a recent post titled "<a title="Yeah Right" href="/images/2013/09/guy_finds_his_house_plumbed_with_beer" target="_blank">Guy finds his house plumbed with beer</a>" grabbed my attention. Of course, upon reading the title, I had to watch the video. I mean a prank involving beer, and a house being plumbed in it? Awesome! So I clicked the video (linked below) and watched it. Within the first few seconds of watching the video, I noticed a few things about it.

<!--more-->

&nbsp;

First, it's a kiwi video! How awesome is that right? Yeah right! Then at 44 seconds, I notice the kegs they are using. They are Tui kegs. Every.Single.One of them. Not only that, but they are very recently painted/refurbished Tui kegs.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/Screen-Shot-2013-09-17-at-4.35.06-PM.png"><img class="size-medium wp-image-465" alt="Tui kegs" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/Screen-Shot-2013-09-17-at-4.35.06-PM.png" width="300" height="160" /></a> Tui kegs

<span style="line-height: 1.714285714; font-size: 1rem;">You can watch the video and decide for yourself if this is an attempt at viral marketing, I'll even embed it for you:</span>

<iframe src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/HG_wfMK7dko" height="315" width="420" allowfullscreen="" frameborder="0"></iframe>

&nbsp;

Have you watched it? Good, because it's totally not viral marketing... Yeah right... Beer in New Zealand is extremely expensive. Certainly too expensive to have some dude "run out for some errands" and come home to find his house plumbed with beer.

&nbsp;

Now let's dig a little deeper into this. They are passing this off as a "we played a prank" on a mate thing. Not as a marketing video. Let that sink in for a second. They are trying to make a viral video to advertise their product. It's shady. Not only that, but, on reddit, someone claims to have been there, and says the <a title="&quot;Proof&quot;" href="/images/2013/09/cc9t1ug" target="_blank">whole thing is totally legit! </a>

&nbsp;

So, let's dig a little deeper. The user has been on Reddit for a little over a year, and the only comments he's made in the last year, is on this video trying to prove it's legit. The logo used for the youtube uploader, is an image of an award the <a title="Tui marketing team wins award " href="/images/2013/09/tui-gnome-highly-commended-award" target="_blank">Tui marketing team won</a>...

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/Screen-Shot-2013-09-17-at-4.22.58-PM.png"><img class="size-full wp-image-466" alt="Youtube icon" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/Screen-Shot-2013-09-17-at-4.22.58-PM.png" width="298" height="250" /></a> Youtube icon

Add to that the fact that the amount of beer they had to have <del datetime="2013-09-17T05:04:52+00:00">used</del> wasted is in the thousands of dollars (just to have a guy come home to find his house plumbed with beer), and the professional wireless cameras, mics and monitors (adding many more thousands of dollars to this "prank"), and it screams of a pro job.

There are then numerous times in the video that it prominently shows the Tui logo. Totally a coincidence... Yeah right... I'm sorry Tui, but this is not a way to gain followers. Yes your prank is cute and all, and you finally found a good use for your poor excuse for beer, but you aren't gaining yourself any more followers. People that drink your beer drink it because it's cheap, not because it's good.

Maybe, you should go back to insulting Christians

[caption id="attachment_468"width="300"]<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/yeahright.png"><img class="size-medium wp-image-468" alt="Tui Ad" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/yeahright.png" width="300" height="148" /></a> Tui Ad

Gay people

[caption id="attachment_469"width="300"]<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/yeahright2.png"><img class="size-medium wp-image-469" alt="Tui Ad" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/yeahright2.png" width="300" height="150" /></a> Tui Ad

Another country

[caption id="attachment_470"width="300"]<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/yeahright3.png"><img class="size-medium wp-image-470" alt="Tui Ad" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/yeahright3.png" width="300" height="125" /></a> Tui Ad

Or possibly the 2,977 people that died on 9/11

[caption id="attachment_471"width="300"]<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/yeahright4.png"><img class="size-medium wp-image-471" alt="Tui Ad" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/09/yeahright4.png" width="300" height="109" /></a> Tui Ad

I'm sure it will convince me to buy your beer... Yeah right.

