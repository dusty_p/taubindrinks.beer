---
title: Taubin Drinks – Mon p’Tit Chou
author: Dustin Plunkett
type: post
date: 2015-04-06T03:44:30+00:00
categories:
  - Beer
  - Beer Porn
  - Taubin Drinks Episodes
tags:
  - Beer
  - Craft beer
  - Garage Project
  - Saison
  - Taubin Drinks
format: video

---
In this episode of Taubin Drinks, I try Garage Project&#8217;s Mon p&#8217;Tit Chou, which means My Little Cabbage in French. It&#8217;s a Belgian Saison. Spoiler alert, it was pretty darn good! Saisons are an &#8220;endangered style&#8221; which fell out of favor with many brewers, but have luckily been making a comeback. They are great for a nice hot day in the summertime.

Check it out in the latest instalment of Taubin Drinks. Cheers!

{{< youtube jGsA4ShOmZ0 >}}

