---
title: Bottling Day!
author: Dustin Plunkett
type: post
date: 2013-04-25T05:21:25+00:00
---
Bottling day is here!

I could hardly sleep last night, as I knew the big day had arrived. I woke up bright and early (actually that's a lie I slept in), bright eyed and bushy tailed and ready to go! I could hardly wait for my wife to wake up so I could get started. Once she woke up and had breakfast, it was time to start! Okay that too is a lie. I waited until after lunch. I figured it was better to bottle on a full stomach than an empty one.

<!--more-->

So, we went and had lunch came back, and got to work. I started out with 3 crates of cleaned, 745 ml bottles.

<a title="Clean Bottles by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679013561"><img title="Clean bottles" alt="Clean Bottles" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679013561_acb538df8a.jpg" width="500" height="333" /></a> Clean bottles

I was quite meticulous in the cleaning of these bottles, I soaked each and every one of them in NapiSan (or for you American's OxyClean) overnight, then put them through a 70 degree  C wash cycle (that's 158 degrees for you Americans), and then let them dry. I then inspected each bottle to make sure it was free of defects (I ended up recycling 3 bottles due to chips).

I then washed my auto siphon and bottling bucket and sanitised both with clean, sanitised water

<a title="Siphoning by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679017421"><img title="Siphoning sanitised water" alt="Siphoning" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679017421_758816e68c.jpg" width="500" height="333" /></a> Siphoning sanitised water

I was so excited at this point, I didn't notice my spigot was leaking (a little more on that later) and moved on to put my caps in sanitised water.

<a title="Caps by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679020843"><img title="Caps sanitising" alt="Caps" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679020843_266da56db9.jpg" width="500" height="333" /></a> Caps sanitising

&nbsp;

With those basic things out of the way, and prepped, it was time to boil the bottling sugar in some water. This will be added to the bottom of the bottling bucket and mixed with the beer for bottling. This is where the carbonation will come from.

<a title="Sugar Boil by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679025561"><img title="Sugar Boil" alt="Sugar Boil" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679025561_daf461a290.jpg" width="500" height="333" /></a> Sugar Boil

Now that everything has been boiled, sanitised and sanitised again, it was time to do the transfer from the fermenter into the bottling bucket.

<a title="Bottle bucket siphon by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8680139680"><img title="Bottle bucket transfer" alt="Bottle bucket siphon" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8680139680_10ba74eb79.jpg" width="333" height="500" /></a> Bottle bucket transfer

The malty smell during the transfer was AMAZING. The transfer went pretty smoothly, and you will notice, there is a clean sterile rag attached to the spigot. That is because it was at about this time we noticed the seal on the bottling bucket wasn't fully sealed and we had leakage. It was a mess, but not too big of a mess that it caused major problems (ie: possible divorce).

<a title="Bottle bucket siphon by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679031145"><img title="Bottle bucket transfer" alt="Bottle bucket siphon" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8679031145_368c119ed3.jpg" width="500" height="333" /></a> Bottle bucket transfer

Things went pretty quickly at this point. Once we had the bottling bucket full of amazing smelling beer, it was time to use the bottling wand to do the actual bottling into the sterilised bottles, then cap them, and move them into the crates for transport to the garage to sit for a few weeks.

<a title="Wife Bottling by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8680154022"><img title="My wife bottling beer" alt="Wife Bottling" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8680154022_1e3d141be8.jpg" width="397" height="500" /></a> My wife helping bottle the beer

The bottling wand and open dishwasher made this a breeze. My wife even got in on the bottling action. You can see some of the beer pooling into the dishwasher door from the leak.

&nbsp;

So after all of that hard work and waiting around, I'm proud to present Cannon Fodder English Bitter!

<a title="Full Bottles by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8680173760"><img title="Cannon Fodder English Bitter" alt="Full Bottles" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8680173760_0c694f713b.jpg" width="500" height="333" /></a> Cannon Fodder English Bitter

My wife came up with the name, and we figured it was fitting for a bottling that happened on <a title="ANZAC Day - Wikipedia " href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/Anzac_day" target="_blank">ANZAC day</a>. It will now sit in the garage for 3 weeks or so, and then I'll try it out and see how it fared. If todays taste is any indication, it should be pretty damned good!

Cheers!

