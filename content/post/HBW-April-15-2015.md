---
title: HBW April 15 2015
author: Dustin Plunkett
type: post
date: 2015-04-15T07:32:24+00:00
categories:
  - Beer
  - Blog
  - HBW
  - Homebrewing
tags:
  - Beer
  - brewing
  - HBW
  - Homebrew
  - Homebrew Wednesday
  - Kegging
---

It&#8217;s Wednesday, and that means, Home Brew Wednesday! In this HBW episode, I talk about my latest (as of yet un-named) beer, the Vanilla Java Stout, as well as a couple of the things that didn&#8217;t go entirely right with brewing this time around. Hold on to your shoelaces, it&#8217;s HBW for April 15th. Cheers!

<!--more-->

{{< youtube CbRyScMNPCg >}}

