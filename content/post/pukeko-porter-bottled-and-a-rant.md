---
title: Pukeko porter bottled and a rant
author: Dustin Plunkett
type: post
date: 2013-08-14T02:13:37+00:00
---

My Pukeko Porter has been bottled! It should be ready to try out in a week or two. The bottling this time was made much easier by the fact I picked up a proper bottling bucket this time. It made things a complete breeze. There was no mess, and no fuss. I had nothing to complain about with that (the rant comes later). In fact, I was able to bottle all 25 bottles solo. It was great.

I took a quick taste at bottling time, and it tasted really good. There are some great coffee notes in the beer, which was amazing. I also learned a bit from my last batch. I let this one sit a lot longer, and let the yeast do its thing for a longer period of time. This helped to tone out the sweetness I had with the <a title="Cannon Fodder week 3 update" href="https://taubindrinks.beer/cannon-fodder-week-3-update/">Cannon Fodder</a> at the beginning.

<!--more-->

&nbsp;

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/Photo-1.jpg"><img class=" wp-image-435   " title="Pukeko Porter cases" alt="Pukeko Porter Cases" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/Photo-1.jpg" width="563" height="421" /></a> Pukeko Porter cases

Overall, I'm very pleased with this beer, and have higher expectations than I did with the last one. I might even label this one, we'll see. I have also made the promise to share this one with some friends, so even if it turns out not so amazing, I have to let others try it. Something I haven't done with the Canon Fodder, as it wasn't a very good representation of the style, and to me was a bit sweet. So, those of you that happen to read this and live close enough to me, be on the lookout for Pukeko Porter.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/pukeko_S.jpg"><img class=" wp-image-438 " alt="Pukeko" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/pukeko_S.jpg" width="563" height="374" /></a> Pukeko porter's namesake

I'll definitely keep you all updated with the status of the porter after I try it out.

<strong>Now for the rant. Stop reading here if you don't want to hear me bitch about greed within the craft beer industry.</strong>

As I write this, a semi-local brewery named Moa is tanking. They made the decision in September of last year to go public in order to expand their brewery, something I, and quite a few other people thought was stupid. I can practically hear some of you questioning why I'd say it's stupid. Well, here is why. Growth within an industry like the craft beer industry should be organic and happen naturally. If you try to force it, bad things will happen. In Moa's case, their stock lost 30% of its value in one day.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/Screen-Shot-2013-08-14-at-11.00.11-AM.png"><img class="size-full wp-image-440" alt="Moa Stock" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/Screen-Shot-2013-08-14-at-11.00.11-AM.png" width="791" height="180" /></a> Moa Stock Tanking

If you do not have an extreme passion for what you do, the consumer will be able to tell. The companies that make it in the craft beer industry, tend to have people at the helm that truly LOVE the industry. They are not in it for a paycheck (though that doesn't hurt). People like Greg Koch and Steve Wagner of Stone brewing, Sam Calagione of Dogfish Head, Tomme Arthur of Lost Abbey, Jim Koch of Sam Adams. The list goes on and on. On the flip side of that, you get a company that is run by a board of directors that knows nothing about the industry.

That is what is happening with Moa. The owner is a former wine maker, who decided to get into craft beer. Don't get me wrong, maybe he likes beer, however, most wine makers I know, would stick to wine as it's what you know. Just because beer and wine are both alcoholic drinks, doesn't mean they are the same. I personally do not see any passion there. Especially with a large board of directors, run by people who were in the wine and spirits industry. Not one of them has ever been in the beer industry. In fact, most of them made vodka before coming to Moa.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/Moa.jpg"><img class=" wp-image-442 " title="Moa original" alt="Moa original" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/Moa.jpg" width="512" height="342" /></a> Moa original

The fact that on their <a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/moa-management-team" target="_blank">management team page</a> they list Josh Scott as being "one of the pioneering founders of the recent craft beer movement in New Zealand" is to me complete bullshit.  I could claim that I am one of the pioneering founders in the recent beer blogging movement in New Zealand. In fact, I'll do just that: I am one of the pioneering founders in the recent beer blogging movement in New Zealand! Since I said it, it has to be true right? I'm a revolutionary!

Not so much. I<span style="font-size: 1rem;"> </span><span style="font-size: 1rem;">can say that with complete confidence that t</span><span style="font-size: 1rem; line-height: 1.714285714;">here are many other craft breweries that were around before Moa, and many more that are better than Moa. And a whole lot more that are more </span>passionate<span style="font-size: 1rem; line-height: 1.714285714;"> about what they do. It reminds me of a conversation I had with my dad recently. There is a new(ish) brewery that opened up near him that everyone has been raving about. He went there, tried the beer, and wasn't overly impressed with it. When he told me this, I said it made sense. The beer they make is quite average, but, because they are local, it's suddenly amazing.</span>

The same thing can be said for Moa. The beer they make is quite average, but because they are local, it suddenly becomes amazing. They themselves have blamed competition for the lack of sales. More and more people are realizing what great beer is, and are expecting that. Everyday more and more breweries open up, making amazing beer and the consumers are the ones that win.

So Moa, if you want my advice, get someone in there that knows about beer instead of a former winemaker and bunch of vodka distillers. Start caring more about your product and less about lining your executives pocketbooks, and you'll start to sell more beer. It's that simple. If you don't, you'll fail, and your stock will continue to plummet. You now have competition, and your consumers don't care about your claims of how you helped pioneer the craft beer movement. They only care about the quality of the product you make.


